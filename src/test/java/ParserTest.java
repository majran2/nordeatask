import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nordea.javaTask.SentenceParserException;
import com.nordea.javaTask.model.Text;
import com.nordea.javaTask.parsers.CsvParser;
import com.nordea.javaTask.parsers.XmlParser;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.reporters.Files;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ParserTest {
    @Test
    public void shouldReturnProperCSVDocument() throws IOException, SentenceParserException {
        //given
        var outputStream = new ByteArrayOutputStream();
        var inputStream = new FileInputStream(new File("src/test/java/small.in"));
        //when
        new CsvParser().parse(inputStream, outputStream);
        //then
        var expected =  Files.readFile(new File("src/test/java/small.csv"));
        var actual = outputStream.toString().replaceAll("\\r\\n?", "\n");
        Assert.assertEquals(expected, actual, "File and output are not equal.");
    }

    @Test
    public void shouldReturnProperXMLDocument() throws IOException, SentenceParserException {
        //given
        var outputStream = new ByteArrayOutputStream();
        var inputStream = new FileInputStream(new File("src/test/java/small.in"));
        //when
        new XmlParser().parse(inputStream, outputStream);
        var xmlMapper = new XmlMapper();
        xmlMapper.setDefaultUseWrapper(false);
        var expected = xmlMapper.readValue(Files.readFile(new File("src/test/java/small.xml")), Text.class);
        var actual = xmlMapper.readValue(outputStream.toString(), Text.class);

        //then
        Assert.assertEquals(xmlMapper.writeValueAsString(expected),
                xmlMapper.writeValueAsString(actual), "File and output are not equal.");
    }
}
