package com.nordea.javaTask;

import com.nordea.javaTask.parsers.CsvParser;
import com.nordea.javaTask.parsers.XmlParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.io.InputStream;

import static java.lang.System.in;
import static java.lang.System.out;

@Slf4j
public class Program {

    private static CommandLine cmd;

    public static void main(String[] args) throws SentenceParserException {

        if(!checkIfInputAvailable(in)) {
            log.error("No input available.");
            return;
        }
        if(!handleCommandLineOptions(args)) return;

        switch (cmd.getOptionValue("f").toLowerCase()){
            case "xml":
                log.info("Using XML parser.");
                new XmlParser().parse(in, out);
                break;
            case "csv":
                log.info("Using CSV parser.");
                new CsvParser().parse(in, out);
                break;
            default:
                log.error("Format not supported.");
        }
     }

     private static boolean handleCommandLineOptions(String[] args){
         Options options = new Options();
         options.addOption("f", true, "define format of output [xml|csv]");
         try {
             cmd = new DefaultParser().parse(options, args);
             if(!cmd.hasOption("f")){
                 log.error("No format defined.");
                 return false;
             }
         } catch (ParseException e) {
             log.error("Cannot parse command line args.");
            return false;
         }
         return true;
     }

     private static boolean checkIfInputAvailable(InputStream in){
         try {
             if (in.available() == 0) {
                 out.println("No input");
                 return false;
             }
         } catch (IOException e) {
             throw new RuntimeException(e);
         }
         return true;
     }
}
