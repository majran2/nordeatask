package com.nordea.javaTask;

public class SentenceParserException extends Exception {
    public SentenceParserException(String msg, Exception e) {
        super(msg,e);
    }
}
