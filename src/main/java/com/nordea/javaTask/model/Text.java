package com.nordea.javaTask.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JacksonXmlRootElement(localName = "text")
public class Text {
    @JacksonXmlProperty(localName = "sentence")
    private List<Sentence> sentences = new ArrayList<>();
}
