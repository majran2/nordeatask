package com.nordea.javaTask.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.util.List;

@Data
@JacksonXmlRootElement(localName = "sentence")
public class Sentence {
    @JacksonXmlProperty(localName = "word")
    private List<String> words;
}
