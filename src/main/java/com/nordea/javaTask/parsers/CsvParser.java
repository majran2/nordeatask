package com.nordea.javaTask.parsers;

import com.nordea.javaTask.model.Sentence;
import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;

@Slf4j
public class CsvParser extends Parser{

    private int sentenceCounter = 1;

    protected void parseStream(InputStream in, OutputStream out) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        ICSVWriter writer =
        new CSVWriterBuilder(new OutputStreamWriter(out))
                .withEscapeChar(CSVWriter.DEFAULT_ESCAPE_CHARACTER)
                .withLineEnd(CSVWriter.RFC4180_LINE_END)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withQuoteChar(CSVWriter.NO_QUOTE_CHARACTER)
                .build();

        Consumer<StringBuilder> write = (sentence) -> {
            var words = extractWords(sentence).getWords();
            log.debug("Words in sentence: " + words.toString());
            writer.writeNext(words.stream().toArray(String[]::new));
            writer.flushQuietly();
        };

        produceOutput(reader, write);
    }

    private Sentence extractWords(StringBuilder currentSentence) {
        String removedPunct = currentSentence.toString()
                .replaceAll("[\\p{Punct}&&[^'’]]", " ")
                .replaceAll("\\s{2,}", " ")
                .trim();
        Sentence sentenceObject = new Sentence();
        var wordsArray = removedPunct.split(" ");
        Arrays.sort(wordsArray);
        var listOfWords = new ArrayList<String>();
        listOfWords.add("Sentence " + sentenceCounter++);
        listOfWords.addAll(Arrays.asList(wordsArray));
        sentenceObject.setWords(listOfWords);
        return sentenceObject;
    }
}
