package com.nordea.javaTask.parsers;

import com.nordea.javaTask.SentenceParserException;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.function.Consumer;

@Slf4j
public abstract class Parser implements ISentenceParser {

    private static final char[] sentenceDelimiters = new char[] {'!','?','.'};
    private static final char[] ignoredChars = new char[] {'\t','\n','\r'};
    private static final String[] sentenceDelimiterExceptions = new String[] {"Mr", "Mrs", "Ms", "Dr"};

    public Parser(){
        Arrays.sort(sentenceDelimiters);
        Arrays.sort(ignoredChars);
        Arrays.sort(sentenceDelimiterExceptions);
    }

    @Override
    public void parse(InputStream in, OutputStream out) throws SentenceParserException {
        try {
            parseStream(in, out);
        } catch (Exception e) {
            log.error("Exception on parsing.", e);
            throw new SentenceParserException("Exception on parsing.", e);
        }
    }

    protected abstract void parseStream(InputStream in, OutputStream out) throws Exception;

    public boolean isSentenceDelimiter(char c){
        return Arrays.binarySearch(sentenceDelimiters, c) >= 0;
    }

    public boolean isExceptionOnDelimiter(String sentence){
        for (var exception: sentenceDelimiterExceptions) {
            if(sentence.endsWith(exception)){
                return true;
            }
        }
        return false;
    }

    public boolean isEndOfSentence(String sentence, char current){
        return isSentenceDelimiter(current)
                && !isExceptionOnDelimiter(sentence);
    }

    public boolean isIgnoredChar(char c){
        return Arrays.binarySearch(ignoredChars, c) >= 0;
    }

    public void produceOutput(BufferedReader reader, Consumer writeOutput) throws IOException {
        StringBuilder currentSentence = new StringBuilder();
        while (true) {

            char current = (char) reader.read();
            if (current == -1 || current == '\uFFFF') {
                log.debug("End of stream reached.");
                break;
            }

            if (isIgnoredChar(current)) {
                current = ' ';
            }

            if (isEndOfSentence(currentSentence.toString(), current)) {
                log.debug("End of sentence. " + currentSentence.toString());
                writeOutput.accept(currentSentence);
                log.debug("Reset sentence buffer.");
                currentSentence.setLength(0);
                continue;
            }

            currentSentence.append(current);
        }
    }
}
