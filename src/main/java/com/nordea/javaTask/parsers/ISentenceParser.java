package com.nordea.javaTask.parsers;

import com.nordea.javaTask.SentenceParserException;

import java.io.InputStream;
import java.io.OutputStream;

public interface ISentenceParser {

    void parse(InputStream in, OutputStream out) throws SentenceParserException;
}
