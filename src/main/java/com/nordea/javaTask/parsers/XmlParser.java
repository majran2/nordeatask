package com.nordea.javaTask.parsers;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nordea.javaTask.model.Sentence;
import lombok.extern.slf4j.Slf4j;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.Arrays;
import java.util.function.Consumer;

@Slf4j
public class XmlParser extends Parser{

    protected void parseStream(InputStream in, OutputStream out) throws XMLStreamException, IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        XMLStreamWriter xmlStreamWriter = XMLOutputFactory.newFactory().createXMLStreamWriter(out);

        xmlStreamWriter.writeStartDocument();
        xmlStreamWriter.writeStartElement("text");

        XmlMapper mapper = new XmlMapper(XMLInputFactory.newFactory());
        mapper.writerWithDefaultPrettyPrinter();
        mapper.setDefaultUseWrapper(false);

        Consumer<StringBuilder> write = (sentenceStringBuilder) -> {

                var sentence = extractWords(sentenceStringBuilder);
                log.debug("Words in sentence: " + sentence.getWords().toString());
            try {
                mapper.writeValue(xmlStreamWriter, sentence);
            } catch (IOException e) {
                log.error("Error while writing sentence. " + e);
            }
        };

        produceOutput(reader, write);

        xmlStreamWriter.writeEndElement();
        xmlStreamWriter.writeEndDocument();
    }

    private Sentence extractWords(StringBuilder currentSentence) {
        String removedPunct = currentSentence.toString().replaceAll("[\\p{Punct}&&[^'’]]", " ").replaceAll("\\s{2,}", " ").trim();
        Sentence sentenceObject = new Sentence();
        sentenceObject.setWords(Arrays.asList(removedPunct.split(" ")));
        return sentenceObject;
    }
}
